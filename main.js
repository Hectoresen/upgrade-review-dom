window.onload = () =>{
    //Descomentar addEventListeners(); para todos los eventos que lo requieran. Excepto en el 1.6
    //addEventListeners();
    //createContent();
    //removeClass();
    //addList();
    //createDiv();
}

//1.1 Basandote en el array siguiente, crea una lista ul > li
//dinámicamente en el html que imprima cada uno de los paises.

/*
const countries = ['Japón', 'Nicaragua', 'Suiza', 'Australia', 'Venezuela'];

const createContent = () =>{
    const ul = document.createElement('ul');
    const parent = document.querySelector('body');
    parent.appendChild(ul);

    countries.forEach(element => {
        let li = document.createElement('li');
        ul.appendChild(li);
        li.innerHTML = element;
    });
}
*/

//1.2 Elimina el elemento que tenga la clase .fn-remove-me.

/*
const removeClass = () =>{
    let remove = document.querySelector('.fn-remove-me');
    remove.remove();
}
*/


//1.3 Utiliza el array para crear dinamicamente una lista ul > li de elementos
//en el div de html con el atributo data-function="printHere".

/*
const cars = ['Mazda 6', 'Ford fiesta', 'Audi A4', 'Toyota corola'];

const div = document.querySelector('div[data-function="printHere"]');

const addList = () =>{
    const ul = document.createElement('ul');
    div.appendChild(ul);

    cars.forEach(element => {
        let li = document.createElement('li');
        ul.appendChild(li);
        li.innerHTML = element;
    });
}
*/

//1.4 Crea dinamicamente en el html una lista de div que contenga un elemento
//h4 para el titulo y otro elemento img para la imagen.

/*
const countries = [
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=1'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=2'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=3'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=4'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=5'}
];

const parent = document.querySelector('body');
const ul = document.createElement('ul');
parent.appendChild(ul);

const createDiv = () =>{
    countries.forEach(element => {
        let div = document.createElement('div');
        ul.appendChild(div);
        let h4 = document.createElement('h4');
        div.appendChild(h4);
        h4.innerHTML = element.title;
        let img = document.createElement('img');
        div.appendChild(img);
        img.src = element.imgUrl;
    });
}
*/

//1.5 Basandote en el ejercicio anterior. Crea un botón que elimine el último
//elemento de la lista.

/*
const countries = [
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=1'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=2'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=3'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=4'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=5'}
];

const parent = document.querySelector('body');
const ul = document.createElement('ul');
parent.appendChild(ul);

const createDiv = () =>{
    countries.forEach(element => {
        let div = document.createElement('div');
        ul.appendChild(div);
        let h4 = document.createElement('h4');
        div.appendChild(h4);
        h4.innerHTML = element.title;
        let img = document.createElement('img');
        div.appendChild(img);
        img.src = element.imgUrl;
    });
}
var createButton = document.createElement('button');
createButton.innerHTML = 'Soy un botón que borra';
parent.appendChild(createButton);

const deleteDiv = () =>{
    ul.lastChild.remove();
}

const addEventListeners = () =>{
    createButton.addEventListener('click', deleteDiv);
}
*/

//1.6 Basandote en el ejercicio anterior. Crea un botón para cada uno de los
//elementos de las listas que elimine ese mismo elemento del html.

/*
const countries = [
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=1'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=2'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=3'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=4'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=5'}
];

const parent = document.querySelector('body');
const ul = document.createElement('ul');
parent.appendChild(ul);


const createDiv = () =>{
    countries.forEach(element => {
        let div = document.createElement('div');
        ul.appendChild(div);
        let h4 = document.createElement('h4');
        div.appendChild(h4);
        h4.innerHTML = element.title;
        let img = document.createElement('img');
        div.appendChild(img);
        img.src = element.imgUrl;
        const createButton = document.createElement('button');
        createButton.innerHTML = 'Borrar esta foto';
        div.appendChild(createButton);

        const removeContainer =()=>{
            createButton.parentNode.remove();
        }
        createButton.addEventListener('click', removeContainer);
    });

}
*/